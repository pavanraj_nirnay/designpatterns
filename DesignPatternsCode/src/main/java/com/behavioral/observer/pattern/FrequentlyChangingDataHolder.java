package com.behavioral.observer.pattern;

import java.util.LinkedHashSet;
import java.util.Set;

public class FrequentlyChangingDataHolder implements ISubject{
	
	private static Set<IFrequentlyChangingDataObserver> frequentlyChangingDataObservers = new LinkedHashSet<IFrequentlyChangingDataObserver>();
	private Object dataA;
	private Object dataB;
	private Object dataC;
	
	private FrequentlyChangingDataHolder() {
	}
	 
	private static class LazyHolder {
		private static final FrequentlyChangingDataHolder INSTANCE = new FrequentlyChangingDataHolder();
	}
	 
	public static FrequentlyChangingDataHolder getInstance() {
	    return LazyHolder.INSTANCE;
	}
	
	public void addObserver(IFrequentlyChangingDataObserver frequentlyChangingDataObserver) {
		frequentlyChangingDataObservers.add(frequentlyChangingDataObserver);
	}

	public void removeObserver(IFrequentlyChangingDataObserver frequentlyChangingDataObserver) {
		frequentlyChangingDataObservers.remove(frequentlyChangingDataObserver);
	}

	public void notifyObservers() {
		for(IFrequentlyChangingDataObserver frequentlyChangingDataObserver:frequentlyChangingDataObservers){
			frequentlyChangingDataObserver.update(this);
		}
	}
	
	public Object getDataA() {
		return dataA;
	}
	public void setDataA(Object dataA) {
		this.dataA = dataA;
	}
	public Object getDataB() {
		return dataB;
	}
	public void setDataB(Object dataB) {
		this.dataB = dataB;
	}
	public Object getDataC() {
		return dataC;
	}
	public void setDataC(Object dataC) {
		this.dataC = dataC;
	}
}
