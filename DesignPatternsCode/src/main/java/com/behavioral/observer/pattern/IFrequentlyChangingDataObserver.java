package com.behavioral.observer.pattern;

public interface IFrequentlyChangingDataObserver {
	public void update(FrequentlyChangingDataHolder dbConfiguration);
}
