package com.behavioral.observer.pattern;

/**
 * Defines one to many dependency between objects.When one object changes state
 * All of its dependents get notified and updated with the change automatically.
 * 
 * Two objects are said to be loosely coupled when they can interact with each other
 * but have very little knowledge of each other.
 * 
 * Observer pattern is similar to publisher and many subscribers, where publisher is
 * called subject.Over here subject and observers are loosely coupled.
 * 
 * The only thing subject knows is that observer implements an interface.
 * New observers can be added to subject at run time with out modifying the subject.
 * 
 * If we haven't used ObserverPattern,we might end up tightly coupling the subject and observer.
 * We might have concrete observers updating the data.New Observers addition might require
 * change in the subject.All these avoided by the use observer pattern.
 * 
 * Define all data that can change in the Subject, Instead of the Subject "pushing" what 
 * has changed to all Observers, each Observer is responsible for "pulling" its particular
 * "window of interest" from the Subject.
 *
 */
public class ObserverPatternMain {
	
	public static void main(String[] args){
		IFrequentlyChangingDataObserver frequentlyChangingDataObserverA = new ProcessOnFrequentlyChangingDataConfigurationA();
		IFrequentlyChangingDataObserver frequentlyChangingDataObserverB = new ProcessOnFrequentlyChangingDataConfigurationB();
		
		FrequentlyChangingDataHolder.getInstance().addObserver(frequentlyChangingDataObserverA);
		FrequentlyChangingDataHolder.getInstance().addObserver(frequentlyChangingDataObserverB);
		
		FrequentlyChangingDataHolder.getInstance().setDataA("OldDataA");
		FrequentlyChangingDataHolder.getInstance().setDataB("OldDataB");
		FrequentlyChangingDataHolder.getInstance().setDataC("OldDataC");
		FrequentlyChangingDataHolder.getInstance().notifyObservers();
		
		FrequentlyChangingDataHolder.getInstance().setDataA("NewDataA");
		FrequentlyChangingDataHolder.getInstance().setDataB("NewDataB");
		FrequentlyChangingDataHolder.getInstance().setDataC("NewDataC");
		FrequentlyChangingDataHolder.getInstance().notifyObservers();
		
		FrequentlyChangingDataHolder.getInstance().setDataA("LatestDataA");
		FrequentlyChangingDataHolder.getInstance().setDataB("LatestDataB");
		FrequentlyChangingDataHolder.getInstance().setDataC("LatestDataC");
		FrequentlyChangingDataHolder.getInstance().notifyObservers();
		
	}

}
