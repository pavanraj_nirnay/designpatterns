package com.behavioral.observer.pattern;

public class ProcessOnFrequentlyChangingDataConfigurationA implements IFrequentlyChangingDataObserver{
	
	private FrequentlyChangingDataHolder dbConfiguration;
	
	public void update(FrequentlyChangingDataHolder dbConfiguration) {
		this.setDbConfiguration(dbConfiguration);
		System.out.println("Updated Configuration on ProcessOnDbConfigurationA");
		System.out.println(dbConfiguration.getDataA().toString());
		System.out.println(dbConfiguration.getDataB().toString());
		System.out.println(dbConfiguration.getDataC().toString());
	}

	public FrequentlyChangingDataHolder getDbConfiguration() {
		return dbConfiguration;
	}
	public void setDbConfiguration(FrequentlyChangingDataHolder dbConfiguration) {
		this.dbConfiguration = dbConfiguration;
	}

}
