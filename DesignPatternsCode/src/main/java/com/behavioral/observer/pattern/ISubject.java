package com.behavioral.observer.pattern;

public interface ISubject {
	public void addObserver(IFrequentlyChangingDataObserver observer);
	public void removeObserver(IFrequentlyChangingDataObserver observer);
	public void notifyObservers();
}
