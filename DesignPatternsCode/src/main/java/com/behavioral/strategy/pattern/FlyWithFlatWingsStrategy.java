package com.behavioral.strategy.pattern;

public class FlyWithFlatWingsStrategy implements IFlyBehaviourStrategy{

	public void fly() {
		System.out.println("Flying with flat wings.");
	}

}
