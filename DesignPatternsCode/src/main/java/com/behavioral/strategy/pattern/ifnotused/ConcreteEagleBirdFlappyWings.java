package com.behavioral.strategy.pattern.ifnotused;

public class ConcreteEagleBirdFlappyWings extends AbstractBird implements ICanFly{
	
	@Override
	public void display() {
		System.out.println("Displaying Eagle Bird.");
	}

	public void fly() {
		System.out.println("Flying with flappy wings.");
	}

}
