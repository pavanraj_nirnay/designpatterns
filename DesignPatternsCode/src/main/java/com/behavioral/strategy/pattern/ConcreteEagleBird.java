package com.behavioral.strategy.pattern;

public class ConcreteEagleBird extends AbstractBird{

	public ConcreteEagleBird(IFlyBehaviourStrategy flyBehaviour) {
		super(flyBehaviour);
	}

	@Override
	public void display() {
		System.out.println("Displaying Eagle Bird.");
	}

}
