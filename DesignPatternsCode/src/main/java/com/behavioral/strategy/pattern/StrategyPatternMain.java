package com.behavioral.strategy.pattern;

/**
 * 
 * Take what varies and encapsulate it so it won't affect rest of code.
 * Flying behaviors for birds might change frequently, so encapsulate it.
 * So later we can alter the flying behavior with out even knowing which 
 * birds encapsulated it.
 * 
 * Behaviors are seperated and encapsulated for flexibility of systems.
 * Bird class doesn't know implementation details of flying behaviors.
 * 
 * Program to an interface rather than concrete implementation.
 * 
 * Programming to an interface.
 * AbstractBird eagleBird = new ConcreteEagleBird(eagleflyBehaviourStrategy).
 * Exploit polymorphisim by assigning concrete implementation object at run time.
 *  
 * Programming to an implementation.
 * ConcreteEagleBird eagleBird = new ConcreteEagleBird(eagleflyBehaviourStrategy).
 * 
 * Ability to reuse flying behaviours and easy to modify flying behvaoir.
 * Prefer composition over inheritance.Flexibility to change behavior run time.
 * 
 * Define a family of algorithms, encapsulate each one, and make them interchangeable. 
 * Strategy lets the algorithm vary independently from the clients that use it.
 * Capture the abstraction in an interface, bury implementation details in derived classes.
 * 
 * Set the flying behavior using a constructor or a setter.
 * 
 */

public class StrategyPatternMain {
	
	public static void main(String[] args){
		
		IFlyBehaviourStrategy eagleflyBehaviourStrategy = new FlyWithFlatWingsStrategy();
		AbstractBird eagleBird = new ConcreteEagleBird(eagleflyBehaviourStrategy);
		eagleBird.display();
		if(eagleBird.canFly())
			eagleBird.fly();
		
		IFlyBehaviourStrategy HummingGirdflyBehaviourStrategy = new FlyWithFlappyWingsStrategy();
		AbstractBird hummingBird = new ConcreteEagleBird(HummingGirdflyBehaviourStrategy);
		hummingBird.display();
		if(hummingBird.canFly())
			hummingBird.fly();
		
	}
	
}
