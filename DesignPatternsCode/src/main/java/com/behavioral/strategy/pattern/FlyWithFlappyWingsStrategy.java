package com.behavioral.strategy.pattern;

public class FlyWithFlappyWingsStrategy implements IFlyBehaviourStrategy{

	public void fly() {
		System.out.println("Flying with flappy wings.");
	}

}
