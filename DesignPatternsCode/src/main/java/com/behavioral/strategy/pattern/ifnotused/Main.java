package com.behavioral.strategy.pattern.ifnotused;

/**
 * 
 * Bird and Behaviour are tightly coupled.Redundant to create multiple eagle bird objects
 * for every behavior.Similarly redundant to hold information of flying behavior in birds.
 * No more programming to an interface, We ended up programming to concrete implementaiton.
 * 
 * AbstractBird bird = new ConcreteEagleBirdFlappyWings();
 * not good because we cannot execute fly operation for AbstractBird.
 * Adding the unintended behavior of fly is also not a good option.
 * 
 * Any change to be done to flying behavior has to be implemented in all the bird Objects.
 * 
 * 
 */

public class Main {
	
	public static void main(String[] args){
		
		ConcreteEagleBirdFlappyWings concreteEagleBirdFlappyWings = new ConcreteEagleBirdFlappyWings();
		concreteEagleBirdFlappyWings.display();
		concreteEagleBirdFlappyWings.fly();
		
		ConcreteEagleBirdFlatWings concreteEagleBirdFlatWings = new ConcreteEagleBirdFlatWings();
		concreteEagleBirdFlatWings.display();
		concreteEagleBirdFlatWings.fly();
	}
	
}
