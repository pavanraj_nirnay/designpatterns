package com.behavioral.strategy.pattern;

public interface IFlyBehaviourStrategy {
	public void fly();
}
