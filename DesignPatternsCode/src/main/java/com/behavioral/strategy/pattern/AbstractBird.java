package com.behavioral.strategy.pattern;

public abstract class AbstractBird {
	
	public IFlyBehaviourStrategy flyBehaviour;
	public AbstractBird(IFlyBehaviourStrategy flyBehaviour){
		this.flyBehaviour=flyBehaviour;
	}
	
	public abstract void display();
	
	public boolean canFly(){
		if(flyBehaviour!=null)
			return true;
		return false;
	}
	
	public void fly(){
		if(flyBehaviour!=null)
			flyBehaviour.fly();
	}
}
